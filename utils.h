#ifndef UTILS_H
#define UTILS_H
#include <iostream>
#include <iomanip>
#include <cctype>
#include <cstring>

/******************************************************************************
 * Aaron Willett
 *
 * Prototypes for utility functions and initial values
*******************************************************************************/

const int I_INIT (0);
const float F_INIT (0.0);
const int STR_BUF (200);
const int IGNORE (1000);

int readInput (int input); // overloaded
float readInput (float input); // overloaded
char readInput (char input); // overloaded
bool readInput (char input[]); // overloaded
int readInput(char options[], int choice); // overloaded
#endif
