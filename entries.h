#include <cstring>
#include <cctype>
#include <iostream>
#include "utils.h"

/********************************************************************************
 * Aaron Willett
 *
 * Prototypes for Entry class and Trip class
 * Prototypes for menu(), seg_again(), trip_again()
*********************************************************************************/
#ifndef ENTRIES_H
#define ENTRIES_H

const int MAX_ERROR{100};
const int STR_MAX{150};
const int SEG_MAX{5};

struct ENTRIES_E 
{
    int error; // error code in the 90's
    char msg[200]{"Error - Something went wrong. Please try again"};
};

class Trip
{
    public: 
        Trip(void);
        ~Trip(void);
        int create_entry(char* name, char* music, char* title, float& length);
        int copy_entry(const Trip & new_trip);
        int remove();
        int retrieve(char* name, Trip& found);
        int retrieve(Trip& found);
        int display(void) const;
    private:
        // data members
        char * name;
        char * music;
        char * time;
        int length;
};

class Segment
{
    public:
        Segment(void);
        ~Segment(void); 
        int create_entry(char* type, char* start, char* end, float& cost);
        int copy_entry(const Segment & n_seg);
        int remove();
        int retrieve(char* type, Segment& found);
        int retrieve(Segment& found);
        int display(void) const;
    private:
        char * type;
        char * start;
        char * end;
        float cost;
};

bool seg_again();
bool trip_again();
int menu();
#endif

