#include "stack.h"

/********************************************************************************
 * Aaron Willett
 * 
 * Stack Implementation (ADT): Constructor, Destructor, and various methods
 * for implementing the class Stack, which utilizes a Linear Linked List 
 *
*********************************************************************************/

Stack::Stack(void)
{
    head = nullptr;
}

Stack::~Stack(void)
{
    // while head is not null, deallocate all dyanamic memory and delete
    // nodes from the stack
    while (head)
    {
        sNode * temp = head->next;
        delete [] head->segments;
        head->segments = nullptr;
        delete head;
        head = temp;
        top_index = 4;
    }
    head = nullptr;
    top_index = 0;
}

//Public---------------------------------------------------------------------------------//

        
int Stack::push(const Segment & n_seg)
{
    STACK_E error;

    if (!head) // list is empty
    {
        top_index = 0;
        head = new sNode;
        head->next = nullptr;
        head->segments = new Segment[SEG_MAX]; // allocate dynamic array with size 5
        // copy passed in Segment object data into dynamic array at index
        head->segments[top_index].copy_entry(n_seg); 
        
        return 1;
    }

    if (top_index == 4) // array is full - create new node
    {
        top_index = 0;
        sNode * nNode = new sNode; // new node allocation
        nNode->next = nullptr;
        nNode->segments = new Segment[SEG_MAX]; // new dynamic array for new node
        nNode->segments[top_index].copy_entry(n_seg); // copying into array
        nNode->next = head; // reassigning new node's next pointer
        head = nNode; // reassigning head pointer the value of the new node

        return 1;
    }

    // adding to top of stack
    head->segments[++top_index].copy_entry(n_seg);
    return 1;
}

int Stack::pop(Segment & removed)
{
    STACK_E error;
    if (!head) // list is empty
    {
        error.error = 80;
        strcpy(error.msg, "Stack list is empty");
        throw error;
    }

    // last index on current node
    if (top_index == 0 && head->next)
    {
        // copy data at current index on dynamic array into 
        // Segment argument to return to client
        removed.copy_entry(head->segments[top_index]);
        sNode * temp = head->next; // hold pointer to next node
        head->segments[top_index].remove(); // deallocate memory at array index
        delete [] head->segments;
        delete head;
        head = temp;
        top_index = 4; // reset top_index to reflect top of next node array

        return 1;
    }

    // last index on array && the last node
    if (top_index == 0 && !head->next)
    {
        removed.copy_entry(head->segments[top_index]);
        head->segments[top_index].remove();
        delete [] head->segments;
        delete head;
        head = nullptr;
        return 1;
    }

    // copies data to be deleted into Segment object to return to client
    removed.copy_entry(head->segments[top_index]);
    // if middle of dynamic array on node
    head->segments[top_index].remove();
    --top_index;
    
    return 1;
}

int Stack::pop()
{
    if (top_index == 0 && head->next)
    {
        // copy data at current index on dynamic array into 
        sNode * temp = head->next; // hold pointer to next node
        head->segments[top_index].remove(); // deallocate memory at array index
        delete head;
        head = temp;
        top_index = 4; // reset top_index to reflect top of next node array

        return 1;
    }

    // last index on array && the last node
    if (top_index == 0 && !head->next)
    {
        head->segments[top_index].remove();
        delete head;
        head = nullptr;
        return 1;
    }

    // if middle of dynamic array on node
    head->segments[top_index].remove();
    --top_index;
    return 1;
}
    


int Stack::peek(Segment & top) const
{
    STACK_E error;
    // list is empty
    if (!head)
    {
        error.error = 80;
        strcpy(error.msg, "Stack peek error. List is empty");
        throw error;
    }

    // peeking at the top index, copying to Segment object
    top.copy_entry(head->segments[top_index]);
    
    return 1;
}


int Stack::display()
{
    STACK_E error;
    int idx = top_index;
    if (!head)
    {
        error.error = 80;
        strcpy(error.msg, "list is empty");
        throw error;
    }

    sNode * curr = head;
    // navigate through nodes
    while (curr)
    {
        // navigate through index of dynamic array
        while (idx >= 0)
        {
            curr->segments[idx].display();
            --idx;
        }
        curr = curr->next;
        idx = 4;
    }

    return 1;
}
