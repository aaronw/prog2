#include "queue.h"
/********************************************************************************
 * Aaron Willett
 * 
 * Queue Implementation (ADT): Constructor, Destructor, and various methods
 * for implementing the class Queue, which utilizes a Circular Linked List
 *
 *********************************************************************************/


Queue::Queue()
{
    rear = nullptr;
}

Queue::~Queue()
{
    /*
    // while rear pointer is not null, loop through circular linked list
    // and delete the node at rear pointer
    while (rear)
    {
        if (rear != rear->next)
        {

            qNode * temp = rear->next;
            delete rear;
            rear = temp;
        }
        else delete rear;
    }

    rear = nullptr; // assuming list is empty, reassign rear to nullptr
    */
    while (!isEmpty()) dequeue();
}

int Queue::enqueue(const Trip & n_trip)
{
    QUEUE_E error;

    // if list is empty
    if (!rear)
    {
        rear = new qNode;
        // copies Trip object argument into qNode 
        rear->trip.copy_entry(n_trip); 
        rear->next = rear; // assigns the circularity of CLL
        return 1;
    }

    // attaching to rear
    qNode * nTrip = new qNode;
    nTrip->trip.copy_entry(n_trip);
    nTrip->next = rear->next;
    rear->next = nTrip;
    rear = nTrip;
    return 1;
}

int Queue::dequeue(Trip & removed)
{
    QUEUE_E error;

    // list is empty
    if (!rear)
    {
        error.error = 70;
        strcpy(error.msg, "List is empty - Dequeue\n");
        throw error;
    }


    // one node CLL
    if (rear == rear->next)
    {
        delete rear;
        rear = nullptr;
        return 1;
    }

    // hold ptr value for next node after front
    qNode * temp = rear->next->next;
    // copies data at interested positon to return to client
    removed.copy_entry(rear->next->trip); 
    delete rear->next;
    rear->next = temp; // reassigns rear's next pointer to held position
    return 1;
}

int Queue::dequeue()
{
    // one node CLL
    if (rear == rear->next)
    {
        delete rear;
        rear = nullptr;
        return 1;
    }

    // hold ptr value for next node after front
    qNode * temp = rear->next->next;
    delete rear->next;
    rear->next = temp; // reassigns rear's next pointer to held position
    return 1;
}

int Queue::peek(Trip & front)
{
    QUEUE_E error;
    // if list is empty
    if (!rear)
    {
        error.error = 70;
        strcpy(error.msg, "list is empty...peeking");
        throw error;
    }

    // copies interested node data into Trip object to return to client
    front.copy_entry(rear->next->trip);

    return 1;
}

int Queue::display() const
{
    QUEUE_E error;
    // list is empty
    if (!rear)
    {
        error.error = 70;
        strcpy(error.msg, "Queue is empty - Display\n");
        throw error;
    }

    if (rear == rear->next)
    {
        rear->trip.display();
        return 1;
    }

    // display entire list
    qNode * curr = rear->next;
    while (curr != rear)
    {
        curr->trip.display();
        curr = curr->next;
    }
    curr->trip.display();

    return 1;
}


bool Queue::isEmpty()
{
    if (!rear)
        return true;
    return false;
}
