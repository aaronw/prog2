#include "entries.h"

/********************************************************************************
 * Aaron Willett
 * 
 * Prototypes for Stack class
 * Definition of sNode for handling nodes on Stack
 * Definition of STACK_E for exception handling in the Stack class
*********************************************************************************/
#ifndef STACK_H
#define STACK_H

struct STACK_E
{
    // Errors will be in the 80's
    int error;
    char msg[STR_MAX]{"Error: stack call"};
};

struct sNode
{
    sNode * next; // next pointer
    Segment * segments; // dynamic array of segments
};


class Stack
{
    public:
        // constructor/destructor
        Stack(void);
        ~Stack(void);
        int display(void);
        int push(const Segment & n_seg);
        int pop(Segment & removed);
        int peek(Segment & top) const;
    private:
        // data members
        sNode * head;
        int top_index;
        int pop();
};

#endif
