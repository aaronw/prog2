#include "utils.h"
#include "stack.h"
#include "queue.h"

using namespace std;

/********************************************************************************
 * Aaron Willett
 * 
 * Application (Client): prompts for user input and sends data to corresponding
 * functions to handle data
 *
 * Trip - organizes data for user's Trip
 *      - create_entry: takes user input and returns organized data
 *      - copy_entry: takes a entry of Trip and copies to another instance
 *      - remove: will delete data from an instance of Trip
 *      - display: will display data in an instance of a Trip to the user
 *
 * Segment - organizes data for user's Segment of Segment
 *      - create_entry: takes user input and returns organized data
 *      - copy_entry: takes a entry of Segment and copies to another instance
 *      - remove: will delete data from an instance of Segment
 *      - display: will display data in an instance of a Segment to the user
 *
 * Stack - organizes Segments in a list
 *      - display: will display all instances of Segment
 *      - push: will add Segment to the list to be stored
 *      - pop: will remove the last Segment added to the list and return it to
 *             client
 *      - peek: takes an empty Segment and will return the Segment with copied
 *              data from the last Segment added to the list
 *
 * Queue - organizes Trips in a list
 *      - display: will display all instances of Trip 
 *      - enqueue: will take an instance of Trip and add it to the Queue list
 *      - dequeue: will take an empty instance of Trip and return the object of
 *                 the first instance of Trip from the Queue list that was deleted
 *      - peek: will take an empty instance of Trip and retun the the object
 *              with the same data as the first instance of Trip on the Queue list
 * 
 * miscellaneous: - menu: displays menu for user, returns integer from user choice
 *      - seg_again: prompts user to continue loop, will loop until user chooses
 *                   not to (specific wording for Segment)
 *      - trip_again: prompts user to continue loop, will loop until user chooses
 *                    not to (specific wording for Trip)
 *      - read_input: recieves and validates user input, (multiple instances for
 *                    different data-types - integer, float, char and char *)
*********************************************************************************/

int main (void)
{
    char name[100], music[100], time[10], type[200], start[150], end[150], menu_choice;
    float length{0}, cost{0.0};
    Trip n_trip, aTrip;
    Segment n_seg, aSeg;
    Stack stack;
    Queue queue;

    do
    {
        cout << "\n\n" << endl;
        menu_choice = menu();

        switch (menu_choice)
        {
            case 1:
                //Add Segment
                do
                {
                    cout << "\n\nTrip Segment!" << endl;
                    cout << setfill('-') << setw(60) << ' ' << endl;
                    cout << setfill(' ');
                    cout << "Enter type of transportation: ";

                    readInput(type); cout << endl;

                    cout << "Enter starting location: ";

                    readInput(start); cout << endl;

                    cout << "Enter ending location: ";

                    readInput(end); cout << endl;

                    cout << "Enter any upfront costs for this segment: ";

                    cost = readInput(cost); cout << endl;

                    try
                    {
                        aSeg.create_entry(type, start, end, cost); // creates Segment object from user input
                        cout << "You've entered:\n" <<endl; 
                        aSeg.display(); cout<<endl<<endl; // displays created Segment object
                        stack.push(aSeg); // add's Segment object to the top of the stack
                        aSeg.remove(); // cleanup
                    }

                    catch (ENTRIES_E error) {
                        cerr << "One or more fields were entered incorrectly. Try again." << endl;
                        cerr << "ERROR CODE: " << error.error << endl;
                        cerr << "MSG: " << error.msg << endl;
                    }

                    catch (STACK_E s_error)
                    {
                        cerr << "error hit exception handler!" << endl;
                        cerr << "ERROR MSG: " << s_error.msg << endl;
                        cerr << "ERROR CODE: " << s_error.error << endl;
                    }

                } while (seg_again());

               break;
                
            case 2:
                //Remove Segment
                cout << "Last added segment: " << endl << endl;
                try {
                    stack.peek(aSeg); // returns Segment object from top of the Stack
                    aSeg.display(); // displays Segment object returned from Stack
                    aSeg.remove(); // cleanup
                    cout << endl << endl;
                    cout << "Removed\n" << endl;
                    stack.pop(aSeg); // returns deleted Segment object from the top of the stack
                    aSeg.remove(); // cleanup
                }
                catch (STACK_E error) {
                    cerr << "Exception handler catch" << endl;
                    cerr << "Error code: " << error.error << endl;
                    cerr << error.msg << endl;
                }
                catch (ENTRIES_E error) {
                    cerr << "One or more fields were entered incorrectly. Try again." << endl;
                    cerr << "ERROR CODE: " << error.error << endl;
                    cerr << "MSG: " << error.msg << endl;
                }
                   
                break;
            case 3:
                //Display Stack
                try {
                    stack.display(); // displays all object from the stack
                }
                catch (STACK_E error) {
                    cerr << "Exception handler catch" << endl;
                    cerr << "Error code: " << error.error << endl;
                    cerr << error.msg << endl;
                }
                break;
            case 4:
                //Add Trip
                do
                {
                    cout << "\n\nTrip Log!" << endl;
                    cout << setfill('-') << setw(60) << ' ' << endl;
                    cout << setfill(' ');

                    cout << "Enter name: ";

                    readInput(name); cout << endl;

                    cout << "Enter music you listened to: ";

                    readInput(music); cout << endl;

                    cout << "Enter time trip started: ";

                    readInput(time); cout << endl;

                    cout << "Enter length of journey (in miles): ";

                    length = readInput(length); cout << endl;

                    try {
                        // queue
                        aTrip.create_entry(name, music, time, length); // creation of Trip object from user input
                        n_trip.copy_entry(aTrip);
                        cout << "You've entered" <<endl;
                        n_trip.display(); // display's created Trip object
                        cout << endl << endl;
                        queue.enqueue(n_trip); // adds Trip object to the queue
                        // clean-up
                        aTrip.remove(); // cleanup
                        n_trip.remove();
                    }

                    catch (ENTRIES_E error) {
                        cerr << "One or more fields were entered incorrectly. Try again." << endl;
                        cerr << "ERROR CODE: " << error.error << endl;
                        cerr << "MSG: " << error.msg << endl;
                    }


                    catch (QUEUE_E q_error)
                    {
                        cerr << "error hit exception handler!" << endl;
                        cerr << "ERROR MSG: " << q_error.msg << endl;
                        cerr << "ERROR CODE: " << q_error.error << endl;
                    }

                } while (trip_again());


                break;
            case 5:
                //Remove Trip
                cout << "Last added trip: " << endl << endl;
                try {
                    queue.peek(aTrip); // returns object from queue
                    aTrip.display(); // display returned object from peek
                    aTrip.remove(); // cleanup
                    cout << endl << endl;
                    cout << "Removed\n" << endl;
                    queue.dequeue(aTrip); // returns object from queue while deleting from CLL
                    aTrip.remove(); // cleanup
                }
                catch (QUEUE_E error) {
                    cerr << "Exception handler catch" << endl;
                    cerr << "Error code: " << error.error << endl;
                    cerr << error.msg << endl;
                }
                catch (ENTRIES_E error) {
                    cerr << "One or more fields were entered incorrectly. Try again." << endl;
                    cerr << "ERROR CODE: " << error.error << endl;
                    cerr << "MSG: " << error.msg << endl;
                }
                   
                break;
            case 6:
                //Display Queue
                try {
                    cout << "Queue:" << endl;
                    queue.display(); // displays all objects on the Queue
                }
                catch (QUEUE_E error) {
                    cerr << "Exception handler catch" << endl;
                    cerr << "Error code: " << error.error << endl;
                    cerr << error.msg << endl;
                }
 
                break;
            case 7:
                // QUIT
                cout << "End testing\n\n" << endl;
                break;
            default: cout << "Must enter menu number. Try again.\n\n" << endl;
                     break;
        }
    } while (menu_choice != 7);

    return 0;
}

