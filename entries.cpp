#include "entries.h"
#include "utils.h"
using namespace std;

/********************************************************************************
 * Aaron Willett
 * 
 * Trip Implementation (ADT): Constructor, Destructor, and various methods
 * for implementing the class Trip
 * 
 * Segment Implementation (ADT): Constructor, Destructor, and various methods
 * for implementing the class Segment
 *
*********************************************************************************/

Trip::Trip(void)
{
    name = nullptr;
    music = nullptr;
    time = nullptr;
}

Trip::~Trip(void)
{ 
    // if data members exist, delete dynamically allocated memory 
    // and reassign to nullptr
    if (name)
    {
        delete [] name;
        name = nullptr;
    }
    if (music)
    {
        delete [] music;
        music = nullptr;
    }
    if (time)
    {
        delete [] time;
        time = nullptr;
    }
}

int Trip::create_entry(char* t_name, char* t_music, char* t_time, float& t_length)
{
    ENTRIES_E empty;
    // if any data member is empty, throw exception
    if ((t_name[0] == '\0') || (t_music[0] == '\0') ||
            (t_time[0] == '\0')  || (t_length < 0))
    {
        empty.error = 98;
        strcpy(empty.msg, "Empty values in create_entry");
        throw empty;
    }
    else  // else assign all data to object instance
    {
        name = new char [strlen(t_name) + 1];
        strcpy(name, t_name);
        music = new char [strlen(t_music) + 1];
        strcpy(music, t_music);
        time = new char [strlen(t_time) + 1];
        strcpy(time, t_time);
        length = t_length;
    }

    return 1;
}

int Trip::copy_entry(const Trip & new_trip)
{
    ENTRIES_E empty;

    // if any data member is empty, throw exception
    if ((new_trip.name[0] == '\0') || (new_trip.music[0] == '\0') ||
            (new_trip.time[0] == '\0'))  
    {
        empty.error = 99;
        strcpy(empty.msg, "Empty values in new_trip");
        throw empty;
    }  // else assign all data to object instance
    else 
    {
        name = new char [strlen(new_trip.name) + 1];
        strcpy(name, new_trip.name);
        music = new char [strlen(new_trip.music) + 1];
        strcpy(music, new_trip.music);
        time = new char [strlen(new_trip.time) + 1];
        strcpy(time, new_trip.time);
        length = new_trip.length;
    }

    return 1;

}

int Trip::display(void) const
{
    cout << "\nName: " << name << endl;
    cout << "Trip music: " << music << endl;
    cout << "Start time: " << time << endl;
    cout << "Length: " << length << " miles" << endl;
    
    return 1;
}

int Trip::remove(void)
{
    if (name)
    {
        delete [] name;
        name = nullptr;
    }
    if (music)
    {
        delete [] music;
        music = nullptr;
    }
    if (time)
    {
        delete [] time;
        time = nullptr;
    }
    return 1;
}

/*
int Trip::retrieve(char* matching_name, Trip& found)
{
    ENTRIES_E error;
    if (matching_name[0] == '\0')
    {
        error.error = 90;
        strcpy(error.msg, "trip retrieval failed");
        throw error;
    }

    if (strcmp(matching_name, name) == 0)
    {
        strcpy(found.name, name);
        strcpy(found.music, music);
        found.time = time;
        found.length = length;

        return 1;
    }

    return 0; 
}


*/
/*********************************************************************************/


Segment::Segment(void)
{
    type = nullptr;
    start = nullptr;
    end = nullptr;
}

Segment::~Segment(void)
{
    // if data members exist, delete dynamically allocated memory 
    // and reassign to nullptr
    if (type)
    {
        delete [] type;
        type = nullptr;
    }

    if (start)
    {
        delete [] start;
        start = nullptr;
    }

    if (end)
    {
        delete [] end;
        end = nullptr;
    }
}

int Segment::create_entry(char* t_type, char* t_start, char* t_end, float& t_cost)
{
    ENTRIES_E empty;
    // if any data member is empty, throw exception
    if ((t_type[0] == '\0') || (t_start[0] == '\0') ||
            (t_end[0] == '\0' || t_cost < 0))

    {
        empty.error = 99;
        strcpy(empty.msg, "Empty values in creating seg entry");
        throw empty;
    }
    else   // else assign all data to object instance
    {
        type = new char [strlen(t_type) + 1];
        strcpy(type, t_type);
        start = new char [strlen(t_start) + 1];
        strcpy(start, t_start);
        end = new char [strlen(t_end) + 1];
        strcpy(end, t_end);
        cost = t_cost;
    }

    return 1;
}

int Segment::copy_entry(const Segment & n_seg)
{
    ENTRIES_E empty;
    // if any data member is empty, throw exception
    if ((n_seg.type[0] == '\0') || (n_seg.start[0] == '\0') ||
            (n_seg.end[0] == '\0' || n_seg.cost < 0))
    {
        empty.error = 96;
        strcpy(empty.msg, "Empty values in new_seg");
        throw empty;
    }
    else   // else assign all data to object instance
    {
        type = new char [strlen(n_seg.type) + 1];
        strcpy(type, n_seg.type);
        start = new char [strlen(n_seg.start) + 1];
        strcpy(start, n_seg.start);
        end = new char [strlen(n_seg.end) + 1];
        strcpy(end, n_seg.end);
        cost = n_seg.cost;
    }

   
    return 1;
}

int Segment::display(void) const
{
    cout << "\nTransit type: " << type << endl;
    cout << "Starting location: " << start << endl;
    cout << "End location: " << end << endl;
    cout << "Upfront cost: $" << cost << endl;
    return 1;
}

int Segment::remove(void)
{
    if (type)
    {
        delete [] type;
        type = nullptr;
    }
    if (start)
    {
        delete [] start;
        start = nullptr;
    }
    if (end)
    {
        delete [] end;
        end = nullptr;
    }
    return 1;
}



/*
int Segment::retrieve(char* matching_type, Segment& found)
{
    if (strcmp(matching_type, type) == 0)
    {
        strcpy(found.type, type);
        strcpy(found.start, start);
        strcpy(found.end, end);
        found.cost = cost;

        return 1;
    }

    return 0;
}

*/

bool seg_again(void) // my attempt at recreating Karla's again();
{
    char entered;
       
    cout << "Would you like to enter another segment?\n>> ";
    cin >> entered; cin.ignore(IGNORE, '\n');
    entered = toupper(entered);

    while (entered != 'Y' && entered != 'N')
    {
        cout << "Please enter Yes or No" << endl;
        cin.clear();
        cin >> entered; cin.ignore(IGNORE, '\n');
        entered = toupper(entered);
    }

    if (entered == 'N')
        return false;
    return true;
}

bool trip_again(void) // my attempt at recreating Karla's again();
{
    char entered;
       
    cout << "Would you like to enter another trip?\n>> ";
    cin >> entered; cin.ignore(IGNORE, '\n');
    entered = toupper(entered);

    while (entered != 'Y' && entered != 'N')
    {
        cout << "Please enter Yes or No" << endl;
        cin.clear();
        cin >> entered; cin.ignore(IGNORE, '\n');
        entered = toupper(entered);
    }

    if (entered == 'N')
        return false;
    return true;
}

int menu(void)
{
    int choice {0};
 
     char options[STR_BUF]{"Menu~\n1. Add Segment\n2. "
        "Remove last segment\n3. Display all segments\n4. Add Trip\n5. Remove first Trip\n"
        "6. Display all trips"
        "\n7. Quit\n\n>> "};
      return readInput(options, choice);
}
