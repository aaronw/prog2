#include "utils.h"
using namespace std;
/******************************************************************************
 * @Name: Aaron Willett
 * @Date: 02.4.24
 *
 * @Description:
*******************************************************************************/
/* [overloaded] 
 * displays prompt to user, reads int & validates */
int readInput (int input) // overloaded
{
	int temp = I_INIT;

	cin >> temp;
	cin.ignore(IGNORE, '\n');

	while (!cin || temp < 0)
	{
		cout << "Invalid number. Try again.\n\n";
		cin.clear();
                cin.ignore(IGNORE, '\n');
		cout << ">> ";
		cin >> temp;
		cin.ignore(IGNORE, '\n');
	}

	input = temp;
        return input;
}

/* [overloaded] 
 * displays prompt to user, reads float & validates */
float readInput (float input) // overloaded
{
	float temp = F_INIT;

	cin >> temp;
	cin.ignore(IGNORE, '\n');

	while (!cin || temp < 0)
	{
		cout << "Invalid number. Try again.\n\n";
		cin.clear();
		cout << ">> ";
		cin >> temp;
		cin.ignore(IGNORE, '\n');
	}

	input = temp;
        return input;
}

/* [overloaded] 
 * displays prompt to user, reads char & validates */
char readInput (char input) // overloaded
{
	char temp = ' ';

	cin >> temp;
	cin.ignore(IGNORE, '\n');

	while (!cin || input == '\n')
	{
		cout << "Invalid input. Try again.\n\n";
		cin.clear();
		cout << ">> ";
		cin >> temp;
		cin.ignore(IGNORE, '\n');
	}

	input = temp;
        return input;
}

/* [overloaded] 
 * displays prompt to user, reads char string & validates */
bool readInput (char input[]) // overloaded
{
	char temp[STR_BUF];
	int len = I_INIT;

	cin.getline(temp, STR_BUF);
	len = strlen(temp);

	while (len > STR_BUF || temp[0] == '\0')
	{
		cout << "Invalid entry. Try again.\n\n";
		cin.clear();
                cout << ">> ";
		cin.getline(temp, STR_BUF);
		len = strlen(temp);
	}

	strcpy(input, temp);
        return true;
}

int readInput (char prompt[], int input) // overloaded
{
	int temp = I_INIT;

	cout << prompt;
	cin >> temp;
	cin.ignore(IGNORE, '\n');

	while (!cin || temp < 0)
	{
		cout << "Invalid number. Try again.\n\n";
		cin.clear();
		cin.ignore(IGNORE, '\n');
		cout << prompt;
		cin >> temp;
		cin.ignore(IGNORE, '\n');
	}
	input = temp;
        return input;
}


