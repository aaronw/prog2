#include "entries.h"

/********************************************************************************
 * Aaron Willett
 *
 * Prototypes for Queue class
 * Prototypes for Queue class
 * Definition of qNode for handling nodes on Queue
 * Definition of QUEUE_E for exception handling in the Queue class
*********************************************************************************/

#ifndef QUEUE_H
#define QUEUE_H

struct QUEUE_E
{
    // Queue errors will be in the 70's
    int error;
    char msg[MAX_ERROR]{"Error: Queue error"};
};

struct qNode
{
    qNode * next; // next ptr 
    Trip trip; // initiation of Trip object
};

class Queue
{
    public:
        // methods
        Queue();
        ~Queue();
        int display() const;
        int enqueue(const Trip & n_trip);
        int dequeue(Trip & removed);
        int peek(Trip & front);
        bool isEmpty();
    private:
        // data members
        qNode * rear; // circular linked list
        int dequeue();
};


#endif
