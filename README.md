<h1>Program 2 on stacks and queues!</h1>
<!------------------------------------>
<h3><i>Feb 1,2024</i></h3>
<p>Program initiated on gitlab, cloned to linux server.<br>
Writing .h files for stack and queue.</p>

<h3><i>Feb 3, 2024</i></h3>
<p>Caught up on the labs, and reworked my header file to use what I learned.</br>
Hopefully things go more smoothly for prog2 vs prog1!</p>


<h3><i>Feb 4, 2024</i></h3>
<p>Implemented all my useful util functions to keep readiblity and validate. My two</br>
classes for Trip and Segement are up and running. I may regret making the classes later</br>
but for now they will remain. The labs have really given me the confidence in implementing</br>
more sophisticated code. Next goal is to build Stack functionality. It will use a dynamic</br>
array of Segment objects. Woof.</p>

<h3><i>Feb 5, 2024</i></h3>
<p>Stack is working!! Push, Pop, Peek and Display! Time to work on Queue. Hopefully done</br>
in a few hours.</br></p>

<h3><i>Feb 8, 2024</i></h3>
<p>Got help from Trae, realized I wasn't properly deallocating memory from my dynamic</br>
arrays on the current node. Rectified, and going to keep building. Will hopefully</br>
be complete by the weekend.</p>

<h3><i>Feb 9, 2024</i></h3>
<p>Still debugging. Phew.</p>

<h3><i>Feb 11, 2024</i></h3>
<p>So...The program does what it is meant to do from the user's perspective. BUT - </br>
MEMORY LEAKS GALORE!!!</br>
Known leaks at this moment:</br>
1. When adding 1 segment to the stack and not popping it off, loses an equal amount</br>
of bytes as what was entered as user input.</br>

2. When adding multiple segments to the stack and popping them off, definitely loses</br>
memory around the Stack::push() and Segment::copy_entry() functions of 168 bytes exactly.</br>

3. When adding any Trips to the Queue without dequeueing them before quitting the program</br>
will result in a LOT of leaks. It looks like the destructor is incorrect. It </br>
<i>SEEMS</i> okay to me, but maybe I need to write an inner while loop to go through and </br>
delete all the dynamic memory. That means the Trip destructor isn't being called at the </br>
end of the program. </br>

4. Going to keep tweaking, hopefully something reveals itself.</p>
<------------------------------------------------------------------>

<p> Queue is working w/o leaks! Need to pin down the last leak in Stack. Right now it </br>
it only leaks when all nodes are popped off the stack. SO CLOSE. </p>
<p> WE DID IT, FAM. DRINKS ON ME </p>
